﻿using BaiKiemTra.Models;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace baikiemtra.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public List<Customer>?  Customers { get; set; }
        [JsonIgnore]
        public report?  report { get; set; }
    }
}
