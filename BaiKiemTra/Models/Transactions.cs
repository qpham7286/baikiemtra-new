﻿using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using BaiKiemTra.Models;

namespace baikiemtra.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public List<Customer>? Customers { get; set; }
        [JsonIgnore]
        public List<Employees>?  Employees { get; set; }
        [JsonIgnore]
        public Logs? Logs { get; set; }
        [JsonIgnore]
        public report?  Report { get; set; }


    }
}
